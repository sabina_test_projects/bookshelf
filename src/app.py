import json
import os
from contextlib import asynccontextmanager
from datetime import datetime

from aiohttp import web
from sqlalchemy import select
from sqlalchemy.ext.asyncio import create_async_engine, AsyncSession
from sqlalchemy.orm import sessionmaker

from models import Book
from settings import DATABASE_URL

router = web.RouteTableDef()
engine = create_async_engine(DATABASE_URL, echo=True, future=True)


def async_session_generator():
    return sessionmaker(engine, class_=AsyncSession, expire_on_commit=False)


@asynccontextmanager
async def get_session():
    try:
        async_session = async_session_generator()
        async with async_session() as session:
            yield session
    except Exception:
        await session.rollback()
        raise
    finally:
        await session.close()


@router.get("/")
async def root(_: web.Request) -> web.Response:
    return web.Response(text="Welcome to bookshelf!")


@router.get("/api")
async def api_list_books(request: web.Request) -> web.Response:
    query = select(Book)
    if name := request.rel_url.query.get('name'):
        query = query.where(Book.name == name)
    if author := request.rel_url.query.get('author'):
        query = query.where(Book.author == author)
    if date_published := request.rel_url.query.get('date_published'):
        query = query.where(Book.date_published == date_published)
    if genre := request.rel_url.query.get('genre'):
        query = query.where(Book.genre == genre)

    async with get_session() as session:
        books = (await session.scalars(query)).all()

    result = [{"id": book.id, "name": book.name, "author": book.author, "date_published": str(book.date_published),
               "genre": book.genre} for book in books]
    return web.json_response({"status": "ok", "data": result})


@router.post("/api")
async def api_create_book(request: web.Request) -> web.Response:
    name, author, date_published, genre, filename = None, None, None, None, None
    async for field in (await request.multipart()):
        if field.name == 'name':
            name = (await field.read()).decode()
        if field.name == 'author':
            author = (await field.read()).decode()
        if field.name == 'date_published':
            date_published = (await field.read()).decode()
        if field.name == 'genre':
            genre = (await field.read()).decode()

        if field.name == 'file':
            filename = field.filename
            size = 0
            with open(os.path.join("books", filename), 'wb') as f:
                while True:
                    chunk = await field.read_chunk()
                    if not chunk:
                        break
                    size += len(chunk)
                    f.write(chunk)

    if not (name and author and date_published):
        response_obj = {'status': 'failed', 'reason': "Fields 'name', 'author' and 'date_pulished' are required",
                        'code': 400}
        return web.Response(text=json.dumps(response_obj), status=400)

    async with get_session() as session:
        book = Book(name=name, author=author, date_published=datetime.strptime(date_published, "%Y-%m-%d"), genre=genre,
                    filename=filename)
        session.add(book)
        await session.commit()

    return web.json_response(
        {
            "status": "ok",
            "data": {
                "id": book.id,
                "name": name,
                "author": author,
                "date_published": date_published,
                "genre": genre,
            },
        }
    )


@router.get("/api/{book_id}")
async def api_get_book(request: web.Request) -> web.Response:
    book_id = request.match_info["book_id"]
    async with get_session() as session:
        book = await session.get(Book, int(book_id))
    return web.FileResponse(os.path.join("books", book.filename))


async def init_app() -> web.Application:
    app = web.Application()
    app.add_routes(router)
    return app


if __name__ == "__main__":
    application = init_app()
    web.run_app(application, port=8000)
