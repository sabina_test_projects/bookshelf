from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, Date

Base = declarative_base()


class Book(Base):
    __tablename__ = 'books'
    id = Column(Integer, primary_key=True)
    name = Column(String)
    author = Column(String)
    date_published = Column(Date)
    genre = Column(String)
    filename = Column(String)

    def __repr__(self):
        return "<Book(id='{}', name='{}', author='{}', date_published={}, genre={})>" \
            .format(self.id, self.name, self.author, self.date_published, self.genre)
