# Bookshelf

## Stack

python, aiohttp, postgres, sqlachemy

## Description
We are making an API service, within which we will give the user access to various literary works.

## Requirements
+ The book file can be saved at your discretion;
+ Create a data structure to store in a database, which in the future will allow access to books by (name, author, date_published, genre);
+ Create create/read endpoints that can allow the user to download books, get a list of books by one of the parameters (name, author, date_published, genre) and access the desired book by id (it can be downloaded or viewed online);
+ For create, add validation to check that the user has specified required fields, such as (name, author, date_published).

## Decor
+ Create a repository on GitLab and upload the code there;
+ Functions must be annotated;
+ The API must be covered by tests;
+ It would be a plus to add docker support so the application can be run locally. And also instructions for it.


## Additional functionality

+ Add an endpoint for accepting and parsing an xls file, which will store two pages. The first of which contains the name, and the second the author of the book. This file is sent by the publishing house and contains those pointers to the book that should be included in the denied list, that is, become unavailable for downloading, but remain available for viewing.